package com.investimento.cdb.DTOS;

import com.investimento.cdb.models.Cliente;
import com.investimento.cdb.models.Produto;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;

public class CadastroCarteiraDTO {

    @NotNull(message = "Informe o valor de aplicação.")
    @DecimalMin(value = "100.00", message = "O valor mínimo de aplicação é R$ 100,00.")
    @Digits(integer = 10, fraction = 2, message = "Informe um valor de aplicação válido.")
    private double valorAplicacao;

    @NotNull(message = "Informe o prazo de aplicação (em meses).")
    @Min(value = 1, message = "O prazo de aplicação não pode ser zero.")
    private int prazoAplicacao;

    @NotNull(message = "Informe o Id do cliente.")
    private long idCliente;

    @NotNull(message = "Informe o Id o produto")
    private long idProduto;

    public CadastroCarteiraDTO() {
    }

    public double getValorAplicacao() {
        return valorAplicacao;
    }

    public void setValorAplicacao(double valorAplicacao) {
        this.valorAplicacao = valorAplicacao;
    }

    public int getPrazoAplicacao() {
        return prazoAplicacao;
    }

    public void setPrazoAplicacao(int prazoAplicacao) {
        this.prazoAplicacao = prazoAplicacao;
    }

    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public long getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }
}
