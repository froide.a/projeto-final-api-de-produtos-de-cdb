package com.investimento.cdb.DTOS;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class CadastroClienteDTO {

    @NotEmpty(message = "Informe o nome do cliente.")
    @Size(min = 3, max = 60, message = "O nome do cliente deve ter entre três e sessenta caracteres.")
    private String nome;

    @CPF(message = "Informe o CPF.")
    private String cpf;

    @Email(message = "Informe o telefone.")
    private String email;

    @NotEmpty(message = "Informe o número do telefone.")
    @Size(min = 13, max = 13, message = "O telefone deve ter o formato 11 11111-1111.")
    private String telefone;

    @NotEmpty(message = "Informe uma senha.")
    private String senha;

    public CadastroClienteDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
