package com.investimento.cdb.DTOS;

import com.investimento.cdb.enuns.GrauDeRisco;

import javax.persistence.Column;
import javax.validation.constraints.*;

public class CadastroProdutoDTO {

    @NotEmpty(message = "Informe o nome do produto.")
    @Size(min = 3, max = 40, message = "O nome do produto deve ter entre três e quarente caracteres.")
    private String nome;

    @NotNull(message = "Informe o percentual de rentabilidade ao ano.")
    @DecimalMin(value = "0.01", message = "O percentual mínimo é de 0,01%.")
    @DecimalMax(value = "100.00", message = "O percentual máximo é de 100,00%.")
    @Digits(integer = 3, fraction = 2, message = "Informe um percentual válido de rentabilidade ao ano.")
    private double percentualRentabilidadeAno;

    @NotNull(message = "Informe o grau de risco (1 - Alto, 2 - Médio ou 3 - Baixo).")
    private GrauDeRisco grauDeRisco;

    @NotNull(message = "Informe o valor mínimo de aplicação.")
    @DecimalMin(value = "100.00", message = "O valor mínimo de aplicação é R$ 100,00.")
    @Digits(integer = 10, fraction = 2, message = "Informe um valor de aplicação válido.")
    private double valorMinimoAplicacao;

    @NotNull(message = "Informe o prazo mínimo de aplicação (em meses).")
    @Min(value = 1, message = "O prazo mínimo de aplicação não pode ser zero.")
    private int prazoMinimoAplicacao;

    @NotNull(message = "Informe o prazo máximo de aplicação (em meses).")
    @Min(value = 1, message = "O prazo máximo de aplicação não pode ser zero.")
    private int prazoMaximoAplicacao;

    public CadastroProdutoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPercentualRentabilidadeAno() {
        return percentualRentabilidadeAno;
    }

    public void setPercentualRentabilidadeAno(double percentualRentabilidadeAno) {
        this.percentualRentabilidadeAno = percentualRentabilidadeAno;
    }

    public GrauDeRisco getGrauDeRisco() {
        return grauDeRisco;
    }

    public void setGrauDeRisco(GrauDeRisco grauDeRisco) {
        this.grauDeRisco = grauDeRisco;
    }

    public double getValorMinimoAplicacao() {
        return valorMinimoAplicacao;
    }

    public void setValorMinimoAplicacao(double valorMinimoAplicacao) {
        this.valorMinimoAplicacao = valorMinimoAplicacao;
    }

    public int getPrazoMinimoAplicacao() {
        return prazoMinimoAplicacao;
    }

    public void setPrazoMinimoAplicacao(int prazoMinimoAplicacao) {
        this.prazoMinimoAplicacao = prazoMinimoAplicacao;
    }

    public int getPrazoMaximoAplicacao() {
        return prazoMaximoAplicacao;
    }

    public void setPrazoMaximoAplicacao(int prazoMaximoAplicacao) {
        this.prazoMaximoAplicacao = prazoMaximoAplicacao;
    }
}
