package com.investimento.cdb.DTOS;

import java.time.LocalDate;

public class RespostaCarteiraClienteDTO {

    private long id;
    private RespostaProdutoDTO produto;
    private LocalDate dataAplicacao;
    private double valorAplicacao;
    private int prazoAplicacao;
    private double valorProjetado;

    public RespostaCarteiraClienteDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RespostaProdutoDTO getProduto() {
        return produto;
    }

    public void setProduto(RespostaProdutoDTO produto) {
        this.produto = produto;
    }

    public LocalDate getDataAplicacao() {
        return dataAplicacao;
    }

    public void setDataAplicacao(LocalDate dataAplicacao) {
        this.dataAplicacao = dataAplicacao;
    }

    public double getValorAplicacao() {
        return valorAplicacao;
    }

    public void setValorAplicacao(double valorAplicacao) {
        this.valorAplicacao = valorAplicacao;
    }

    public int getPrazoAplicacao() {
        return prazoAplicacao;
    }

    public void setPrazoAplicacao(int prazoAplicacao) {
        this.prazoAplicacao = prazoAplicacao;
    }

    public double getValorProjetado() {
        return valorProjetado;
    }

    public void setValorProjetado(double valorProjetado) {
        this.valorProjetado = valorProjetado;
    }
}
