package com.investimento.cdb.DTOS;

import com.investimento.cdb.models.Cliente;
import com.investimento.cdb.models.Produto;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

public class RespostaCarteiraDTO {

    private long id;
    private Date dataAplicacao;
    private double valorAplicacao;
    private int prazoAplicacao;
    private RespostaClienteDTO respostaClienteDTO;
    private RespostaProdutoDTO respostaProdutoDTO;

    public RespostaCarteiraDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDataAplicacao() {
        return dataAplicacao;
    }

    public void setDataAplicacao(Date dataAplicacao) {
        this.dataAplicacao = dataAplicacao;
    }

    public double getValorAplicacao() {
        return valorAplicacao;
    }

    public void setValorAplicacao(double valorAplicacao) {
        this.valorAplicacao = valorAplicacao;
    }

    public int getPrazoAplicacao() {
        return prazoAplicacao;
    }

    public void setPrazoAplicacao(int prazoAplicacao) {
        this.prazoAplicacao = prazoAplicacao;
    }

    public RespostaClienteDTO getRespostaClienteDTO() {
        return respostaClienteDTO;
    }

    public void setRespostaClienteDTO(RespostaClienteDTO respostaClienteDTO) {
        this.respostaClienteDTO = respostaClienteDTO;
    }

    public RespostaProdutoDTO getRespostaProdutoDTO() {
        return respostaProdutoDTO;
    }

    public void setRespostaProdutoDTO(RespostaProdutoDTO respostaProdutoDTO) {
        this.respostaProdutoDTO = respostaProdutoDTO;
    }
}
