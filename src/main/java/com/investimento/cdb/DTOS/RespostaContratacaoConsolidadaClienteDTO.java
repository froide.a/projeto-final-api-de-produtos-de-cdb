package com.investimento.cdb.DTOS;

import java.util.List;

public class RespostaContratacaoConsolidadaClienteDTO {

    private double valorConsolidadoCarteira;
    private ResumoClienteDTO cliente;
    private List<RespostaCarteiraClienteDTO> produtosCarteira;

    public RespostaContratacaoConsolidadaClienteDTO() {
    }

    public double getValorConsolidadoCarteira() {
        return valorConsolidadoCarteira;
    }

    public void setValorConsolidadoCarteira(double valorConsolidadoCarteira) {
        this.valorConsolidadoCarteira = valorConsolidadoCarteira;
    }

    public List<RespostaCarteiraClienteDTO> getProdutosCarteira() {
        return produtosCarteira;
    }

    public void setProdutosCarteira(List<RespostaCarteiraClienteDTO> produtosCarteira) {
        this.produtosCarteira = produtosCarteira;
    }

    public ResumoClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ResumoClienteDTO cliente) {
        this.cliente = cliente;
    }
}
