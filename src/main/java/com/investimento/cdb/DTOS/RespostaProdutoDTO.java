package com.investimento.cdb.DTOS;

import com.investimento.cdb.enuns.GrauDeRisco;

import javax.persistence.Column;

public class RespostaProdutoDTO {

    private long id;
    private String nome;
    private double percentualRentabilidadeAno;
    private String grauDeRisco;
    private double valorMinimoAplicacao;
    private int prazoMinimoAplicacao;
    private int prazoMaximoAplicacao;

    public RespostaProdutoDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPercentualRentabilidadeAno() {
        return percentualRentabilidadeAno;
    }

    public void setPercentualRentabilidadeAno(double percentualRentabilidadeAno) {
        this.percentualRentabilidadeAno = percentualRentabilidadeAno;
    }

    public String getGrauDeRisco() {
        return grauDeRisco;
    }

    public void setGrauDeRisco(String grauDeRisco) {
        this.grauDeRisco = grauDeRisco;
    }

    public double getValorMinimoAplicacao() {
        return valorMinimoAplicacao;
    }

    public void setValorMinimoAplicacao(double valorMinimoAplicacao) {
        this.valorMinimoAplicacao = valorMinimoAplicacao;
    }

    public int getPrazoMinimoAplicacao() {
        return prazoMinimoAplicacao;
    }

    public void setPrazoMinimoAplicacao(int prazoMinimoAplicacao) {
        this.prazoMinimoAplicacao = prazoMinimoAplicacao;
    }

    public int getPrazoMaximoAplicacao() {
        return prazoMaximoAplicacao;
    }

    public void setPrazoMaximoAplicacao(int prazoMaximoAplicacao) {
        this.prazoMaximoAplicacao = prazoMaximoAplicacao;
    }
}
