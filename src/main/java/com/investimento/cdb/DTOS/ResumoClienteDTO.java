package com.investimento.cdb.DTOS;

public class ResumoClienteDTO {

    private long id;
    private String nome;

    public ResumoClienteDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
