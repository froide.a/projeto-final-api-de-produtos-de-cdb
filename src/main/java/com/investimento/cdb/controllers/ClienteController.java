package com.investimento.cdb.controllers;

import com.investimento.cdb.DTOS.CadastroClienteDTO;
import com.investimento.cdb.DTOS.RespostaClienteDTO;
import com.investimento.cdb.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaClienteDTO cadastrarCliente(@Valid @RequestBody  CadastroClienteDTO cadastroClienteDTO) {
        try {
            return clienteService.cadastrarCliente(cadastroClienteDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public List<RespostaClienteDTO> consultarTodosClientes(@RequestParam(name = "cpf", required = false) String cpf) {

        if (cpf != null) {
            try {
                return clienteService.consultarClientePorCpf(cpf);
            } catch (RuntimeException e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
            }
        } else {
            try {
                return clienteService.consultarTodosClientes();
            } catch (RuntimeException e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
            }
        }
    }

    @GetMapping("/{id}")
    public RespostaClienteDTO consultarClientePorId(@PathVariable(name = "id") long id) {
        try {
            return clienteService.consultarClientePorId(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public RespostaClienteDTO atualizarCliente(@PathVariable(name = "id") long id, @Valid @RequestBody  CadastroClienteDTO cadastroClienteDTO) {
        try {
            return clienteService.atualizarCliente(id, cadastroClienteDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void excluirCliente(@PathVariable(name = "id") long id) {
        try {
            clienteService.excluirCliente(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());

        }
    }
}
