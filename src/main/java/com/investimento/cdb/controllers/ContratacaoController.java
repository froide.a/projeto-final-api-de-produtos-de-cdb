package com.investimento.cdb.controllers;
import com.investimento.cdb.DTOS.CadastroCarteiraDTO;
import com.investimento.cdb.DTOS.RespostaCarteiraClienteDTO;
import com.investimento.cdb.DTOS.RespostaContratacaoConsolidadaClienteDTO;
import com.investimento.cdb.services.ContratacaoService;
import com.investimento.cdb.services.ClienteService;
import com.investimento.cdb.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController
@RequestMapping("/contratacao")
public class ContratacaoController {

    @Autowired
    ClienteService clienteService;

    @Autowired
    ProdutoService produtoService;

    @Autowired
    ContratacaoService contratacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaCarteiraClienteDTO cadastrarCliente(@Valid @RequestBody CadastroCarteiraDTO cadastroCarteiraDTO) {
        try {
            return contratacaoService.cadastrarCarteira(cadastroCarteiraDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public RespostaContratacaoConsolidadaClienteDTO listarCarteiraCliente(@PathVariable(name = "id") long id) {
     try {
         return contratacaoService.listarCarteiraCliente(id);
     } catch (RuntimeException e) {
         throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
     }

    }
}
