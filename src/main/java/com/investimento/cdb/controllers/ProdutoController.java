package com.investimento.cdb.controllers;

import com.investimento.cdb.DTOS.CadastroProdutoDTO;
import com.investimento.cdb.DTOS.RespostaProdutoDTO;
import com.investimento.cdb.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaProdutoDTO cadastrarProduto(@Valid @RequestBody CadastroProdutoDTO cadastroProdutoDTO) {
        try {
            return produtoService.cadastrarProduto(cadastroProdutoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public List<RespostaProdutoDTO> consultarTodosProdutos() {
        try {
            return produtoService.consultarTodosProdutos();
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public RespostaProdutoDTO consultarProdutoPorId(@PathVariable(name = "id") long id) {
        try {
            return produtoService.consultarProdutoPorId(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public RespostaProdutoDTO atualizarProduto(@PathVariable(name = "id") long id, @Valid @RequestBody  CadastroProdutoDTO cadastroProdutoDTO) {
        try {
            return produtoService.atualizarProduto(id, cadastroProdutoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void excluirProduto(@PathVariable(name = "id") long id) {
        try {
            produtoService.excluirProduto(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());

        }
    }
}
