package com.investimento.cdb.enuns;

import java.util.HashMap;
import java.util.Map;

public enum GrauDeRisco {

    ALTO("ALTO"),
    MEDIO("MEDIO"),
    BAIXO("BAIXO");


    private String descricao;

    GrauDeRisco(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
