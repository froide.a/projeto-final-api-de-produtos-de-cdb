package com.investimento.cdb.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "contratacoes")
public class Contratacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "dataAplicacao", nullable = false)
    private LocalDate dataAplicacao;

    @Column(name = "valorAplicacao", nullable = false)
    private double valorAplicacao;

    @Column(name = "prazoAplicacao", nullable = false)
    private int prazoAplicacao;

    @OneToOne
    private Cliente cliente;

    @OneToOne
    private Produto produto;

    public Contratacao() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDataAplicacao() {
        return dataAplicacao;
    }

    public void setDataAplicacao(LocalDate dataAplicacao) {
        this.dataAplicacao = dataAplicacao;
    }

    public double getValorAplicacao() {
        return valorAplicacao;
    }

    public void setValorAplicacao(double valorAplicacao) {
        this.valorAplicacao = valorAplicacao;
    }

    public int getPrazoAplicacao() {
        return prazoAplicacao;
    }

    public void setPrazoAplicacao(int prazoAplicacao) {
        this.prazoAplicacao = prazoAplicacao;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
