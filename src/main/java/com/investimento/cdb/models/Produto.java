package com.investimento.cdb.models;

import com.investimento.cdb.enuns.GrauDeRisco;

import javax.persistence.*;
import javax.validation.constraints.Digits;

@Entity
@Table(name = "produtos")
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "nome", nullable = false, unique = true, length = 40)
    private String nome;

    @Column(name = "percentualRentabilidadeAno", nullable = false)
    private double percentualRentabilidadeAno;

    @Column(name = "grauDeRisco", nullable = false)
    @Enumerated(EnumType.STRING)
    public GrauDeRisco grauDeRisco;

    @Column(name = "valorMinimoAplicacao", nullable = false)
    private double valorMinimoAplicacao;

    @Column(name = "prazoMinimoAplicacao", nullable = false)
    private int prazoMinimoAplicacao;

    @Column(name = "prazoMaximoAplicacao", nullable = false)
    private int prazoMaximoAplicacao;

    public Produto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPercentualRentabilidadeAno() {
        return percentualRentabilidadeAno;
    }

    public void setPercentualRentabilidadeAno(double percentualRentabilidadeAno) {
        this.percentualRentabilidadeAno = percentualRentabilidadeAno;
    }

    public GrauDeRisco getGrauDeRisco() {
        return grauDeRisco;
    }

    public void setGrauDeRisco(GrauDeRisco grauDeRisco) {
        this.grauDeRisco = grauDeRisco;
    }

    public double getValorMinimoAplicacao() {
        return valorMinimoAplicacao;
    }

    public void setValorMinimoAplicacao(double valorMinimoAplicacao) {
        this.valorMinimoAplicacao = valorMinimoAplicacao;
    }

    public int getPrazoMinimoAplicacao() {
        return prazoMinimoAplicacao;
    }

    public void setPrazoMinimoAplicacao(int prazoMinimoAplicacao) {
        this.prazoMinimoAplicacao = prazoMinimoAplicacao;
    }

    public int getPrazoMaximoAplicacao() {
        return prazoMaximoAplicacao;
    }

    public void setPrazoMaximoAplicacao(int prazoMaximoAplicacao) {
        this.prazoMaximoAplicacao = prazoMaximoAplicacao;
    }
}
