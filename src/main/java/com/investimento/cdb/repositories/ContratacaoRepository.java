package com.investimento.cdb.repositories;

import com.investimento.cdb.models.Cliente;
import com.investimento.cdb.models.Contratacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContratacaoRepository extends JpaRepository<Contratacao, Long> {

    List<Contratacao> findAllByCliente(Cliente cliente);
}
