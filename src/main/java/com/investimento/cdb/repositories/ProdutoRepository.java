package com.investimento.cdb.repositories;

import com.investimento.cdb.models.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
}
