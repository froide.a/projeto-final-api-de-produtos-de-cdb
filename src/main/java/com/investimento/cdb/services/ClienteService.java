package com.investimento.cdb.services;

import com.investimento.cdb.DTOS.CadastroClienteDTO;
import com.investimento.cdb.DTOS.RespostaClienteDTO;
import com.investimento.cdb.models.Cliente;
import com.investimento.cdb.repositories.ClienteRepository;
import com.investimento.cdb.seguranca.auth.UsuarioAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService implements UserDetailsService {

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;


    public RespostaClienteDTO cadastrarCliente(CadastroClienteDTO cadastroClienteDTO) {
        try {
            Cliente cliente = this.converterCadastroClienteDtoParaCliente(cadastroClienteDTO);
            String senha = cliente.getSenha();
            cliente.setSenha(encoder.encode(senha));
            cliente = clienteRepository.save(cliente);

            RespostaClienteDTO respostaClienteDTO = this.converterClienteParaRespostaClienteDto(cliente);

            return respostaClienteDTO;

        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao incluir cliente: " + e.getMessage());
        }
    }

    public List<RespostaClienteDTO> consultarTodosClientes() {
        try {
            List<RespostaClienteDTO> listaRespostaClienteDTO = new ArrayList<>();
            List<Cliente> listaCliente = clienteRepository.findAll();

            listaRespostaClienteDTO = this.converterListaClienteParaListaClienteDTO(listaCliente);

            return listaRespostaClienteDTO;
        } catch (RuntimeException e ) {
            throw new RuntimeException("Erro ao listar todos os clientes: " + e.getMessage());
        }
    }

    public RespostaClienteDTO consultarClientePorId(Long id) {
        try {
            Optional<Cliente> cliente = clienteRepository.findById(id);

            if (cliente.isPresent()) {
                RespostaClienteDTO respostaClienteDTO = this.converterClienteParaRespostaClienteDto(cliente.get());
                return respostaClienteDTO;
            } else {
                throw new RuntimeException("Id do cliente não encontrado");
            }
        } catch (RuntimeException e ) {
            throw new RuntimeException("Erro ao consultar o cliente por id: " + e.getMessage());
        }
    }

    public List<RespostaClienteDTO> consultarClientePorCpf(String cpf) {
        try {
            List<Cliente> clientes = clienteRepository.findAll();

            if (clientes.size() > 0) {
                List<RespostaClienteDTO> listaRespostaClienteDTO = new ArrayList<>();
                Cliente clientePorCpf = new Cliente();

                clientePorCpf = filtrarClientePorCpf(cpf, clientes);

                if (clientePorCpf != null) {
                    listaRespostaClienteDTO .add(this.converterClienteParaRespostaClienteDto(clientePorCpf));
                    return listaRespostaClienteDTO;
                } else {
                    throw new RuntimeException("Cliente não encontrado.");
                }

            } else {
                throw new RuntimeException("Não há clientes cadastrados.");
            }
        } catch (RuntimeException e ) {
            throw new RuntimeException("Erro ao consultar o cliente por CPF: " + e.getMessage());
        }
    }

    public RespostaClienteDTO atualizarCliente(long id, CadastroClienteDTO cadastroClienteDTO) {
        try {
            Optional<Cliente> cliente = clienteRepository.findById(id);
            if (cliente.isPresent()) {
                Cliente clienteAtualizado = this.converterCadastroClienteDtoParaCliente(cadastroClienteDTO);
                clienteAtualizado.setId(id);
                return this.converterClienteParaRespostaClienteDto(clienteRepository.save(clienteAtualizado));

            } else {
                throw new RuntimeException("Cliente não encontrado para atualização.");
            }
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao consultar o cliente por id: " + e.getMessage());
        }
    }

    public void excluirCliente(long id) {
        try {
            clienteRepository.deleteById(id);
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao excluir o cliente por id: " + e.getMessage());

        }
    }

    private Cliente filtrarClientePorCpf(String cpf, List<Cliente> clientes) {
        for (Cliente cliente: clientes) {
            if (cliente.getCpf().equals(cpf)) {
                return cliente;
            }
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        // Recupera o e-mail cadastrado para o usuário
        Cliente cliente = clienteRepository.findByEmail(email);
        if (cliente == null) {
            throw new UsernameNotFoundException("E-mail não cadastrado");
        }
        UsuarioAuth usuarioAuth = new UsuarioAuth(cliente.getId(), cliente.getEmail(), cliente.getSenha());
        return usuarioAuth;
    }


// Métodos de conversão

    private List<RespostaClienteDTO> converterListaClienteParaListaClienteDTO(List<Cliente> listaCliente) {
        List<RespostaClienteDTO> listaRespostaClienteDTO = new ArrayList<>();

        for (Cliente cliente : listaCliente) {
            RespostaClienteDTO respostaClienteDTO = new RespostaClienteDTO();
            respostaClienteDTO.setId(cliente.getId());
            respostaClienteDTO.setCpf(cliente.getCpf());
            respostaClienteDTO.setTelefone(cliente.getTelefone());
            respostaClienteDTO.setNome(cliente.getNome());
            respostaClienteDTO.setEmail(cliente.getEmail());

            listaRespostaClienteDTO.add(respostaClienteDTO);
        }
        return listaRespostaClienteDTO;
    }

    private RespostaClienteDTO converterClienteParaRespostaClienteDto(Cliente cliente) {
        RespostaClienteDTO respostaClienteDTO = new RespostaClienteDTO();
        respostaClienteDTO.setId(cliente.getId());
        respostaClienteDTO.setNome(cliente.getNome());
        respostaClienteDTO.setCpf(cliente.getCpf());
        respostaClienteDTO.setEmail(cliente.getEmail());
        respostaClienteDTO.setTelefone(cliente.getTelefone());
        return respostaClienteDTO;
    }

    private Cliente converterCadastroClienteDtoParaCliente(CadastroClienteDTO cadastroClienteDTO) {
        Cliente cliente = new Cliente();
        cliente.setNome(cadastroClienteDTO.getNome());
        cliente.setCpf(cadastroClienteDTO.getCpf());
        cliente.setEmail(cadastroClienteDTO.getEmail());
        cliente.setTelefone(cadastroClienteDTO.getTelefone());
        cliente.setSenha(cadastroClienteDTO.getSenha());
        return cliente;
    }


}
