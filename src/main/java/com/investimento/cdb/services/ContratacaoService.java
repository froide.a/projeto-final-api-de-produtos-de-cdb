package com.investimento.cdb.services;

import com.investimento.cdb.DTOS.*;
import com.investimento.cdb.models.Contratacao;
import com.investimento.cdb.models.Cliente;
import com.investimento.cdb.models.Produto;
import com.investimento.cdb.repositories.ContratacaoRepository;
import com.investimento.cdb.repositories.ClienteRepository;
import com.investimento.cdb.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    ContratacaoRepository contratacaoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    ProdutoRepository produtoRepository;

    @Autowired
    ClienteService clienteService;

    @Autowired
    ProdutoService produtoService;

    public RespostaCarteiraClienteDTO cadastrarCarteira(CadastroCarteiraDTO cadastroCarteiraDTO) {
        try {

            Contratacao contratacao = this.converterCadastroCarteiraDtoParaCadastro(cadastroCarteiraDTO);

            // Complementar os dados da carteira
            contratacao.setDataAplicacao(LocalDate.now());

            double rentabilidadeAno = 0;

            // Obter os dados do cliente
            try {
                Optional<Cliente> cliente = clienteRepository.findById(cadastroCarteiraDTO.getIdCliente());

                if (cliente.isPresent()) {
                    contratacao.setCliente(cliente.get());
                } else {
                    throw new RuntimeException("Id do cliente não encontrado");
                }
            } catch (RuntimeException e) {
                throw new RuntimeException("Erro ao consultar o cliente por id: " + e.getMessage());
            }

            // Obter os dados do produto
            try {
                Optional<Produto> produto = produtoRepository.findById(cadastroCarteiraDTO.getIdProduto());

                if (produto.isPresent()) {
                    rentabilidadeAno = produto.get().getPercentualRentabilidadeAno();
                    contratacao.setProduto(produto.get());
                } else {
                    throw new RuntimeException("Id do produto não encontrado");
                }
            } catch (RuntimeException e ) {
                throw new RuntimeException("Erro ao consultar o produto por id: " + e.getMessage());
            }

            contratacao = contratacaoRepository.save(contratacao);

            double valorProjetado = contratacao.getValorAplicacao() * Math.pow(1 + ((rentabilidadeAno / 12) / 100), contratacao.getPrazoAplicacao());

            RespostaCarteiraClienteDTO respostaCarteiraClienteDTO = this.converterCarteiraParaRespostaCarteiraDto(contratacao, valorProjetado);

            return respostaCarteiraClienteDTO;

        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao incluir cliente: " + e.getMessage());
        }
    }

    public RespostaContratacaoConsolidadaClienteDTO listarCarteiraCliente(long id) {
        RespostaContratacaoConsolidadaClienteDTO respostaContratacaoConsolidadaClienteDTO = new RespostaContratacaoConsolidadaClienteDTO();

        double valorConsolidadoCarteira = 0;

        // Obter os dados do cliente
        try {
            Optional<Cliente> cliente = clienteRepository.findById(id);

            if (cliente.isPresent()) {
                ResumoClienteDTO resumoClienteDTO = new ResumoClienteDTO();

                resumoClienteDTO.setId(cliente.get().getId());
                resumoClienteDTO.setNome(cliente.get().getNome());

                respostaContratacaoConsolidadaClienteDTO.setCliente(resumoClienteDTO);

                List<Contratacao> contratacoes = contratacaoRepository.findAllByCliente(cliente.get());

                List<RespostaCarteiraClienteDTO> listaRespostaCarteiraClienteDTO = new ArrayList<>();

                for (Contratacao contratacao : contratacoes) {
                    RespostaCarteiraClienteDTO respostaCarteiraClienteDTO = new RespostaCarteiraClienteDTO();
                    respostaCarteiraClienteDTO.setId(contratacao.getId());
                    respostaCarteiraClienteDTO.setDataAplicacao(contratacao.getDataAplicacao());
                    respostaCarteiraClienteDTO.setPrazoAplicacao(contratacao.getPrazoAplicacao());
                    respostaCarteiraClienteDTO.setValorAplicacao(contratacao.getValorAplicacao());

                    //respostaCarteiraClienteDTO.setValorProjetado();

                    RespostaProdutoDTO respostaProdutoDTO = new RespostaProdutoDTO();
                    respostaProdutoDTO.setId(contratacao.getProduto().getId());
                    respostaProdutoDTO.setValorMinimoAplicacao(contratacao.getProduto().getValorMinimoAplicacao());
                    respostaProdutoDTO.setPrazoMinimoAplicacao(contratacao.getProduto().getPrazoMinimoAplicacao());
                    respostaProdutoDTO.setPercentualRentabilidadeAno(contratacao.getProduto().getPercentualRentabilidadeAno());
                    respostaProdutoDTO.setPrazoMaximoAplicacao(contratacao.getProduto().getPrazoMaximoAplicacao());
                    respostaProdutoDTO.setGrauDeRisco(contratacao.getProduto().getGrauDeRisco().getDescricao());
                    respostaProdutoDTO.setNome(contratacao.getProduto().getNome());


                    double valorProjetado = contratacao.getValorAplicacao() * Math.pow(1 + ((contratacao.getProduto().getPercentualRentabilidadeAno() / 12) / 100), contratacao.getPrazoAplicacao());

                    respostaCarteiraClienteDTO.setValorProjetado(formatarDouble(valorProjetado));


                    respostaCarteiraClienteDTO.setProduto(respostaProdutoDTO);

                    LocalDate dataAtual = LocalDate.now();
                    LocalDate dataContratacao = contratacao.getDataAplicacao();
                    long qtdMeses = ChronoUnit.MONTHS.between(dataAtual, dataContratacao);

                    valorConsolidadoCarteira +=
                            contratacao.getValorAplicacao()
                            * Math.pow(1 + ((contratacao.getProduto().getPercentualRentabilidadeAno() / 12) / 100), qtdMeses);

                    listaRespostaCarteiraClienteDTO.add(respostaCarteiraClienteDTO);
                }

                respostaContratacaoConsolidadaClienteDTO.setValorConsolidadoCarteira(valorConsolidadoCarteira);
                respostaContratacaoConsolidadaClienteDTO.setProdutosCarteira(listaRespostaCarteiraClienteDTO);

                return respostaContratacaoConsolidadaClienteDTO;
            } else {
                throw new RuntimeException("Id do cliente não encontrado");
            }
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao consultar o cliente por id: " + e.getMessage());
        }

    }

    private RespostaCarteiraClienteDTO converterCarteiraParaRespostaCarteiraDto(Contratacao contratacao, double valorProjetado) {
        RespostaCarteiraClienteDTO respostaCarteiraClienteDTO = new RespostaCarteiraClienteDTO();
        respostaCarteiraClienteDTO.setId(contratacao.getId());
        respostaCarteiraClienteDTO.setDataAplicacao(contratacao.getDataAplicacao());
        respostaCarteiraClienteDTO.setPrazoAplicacao(contratacao.getPrazoAplicacao());
        respostaCarteiraClienteDTO.setValorAplicacao(contratacao.getValorAplicacao());
        respostaCarteiraClienteDTO.setValorProjetado(this.formatarDouble(valorProjetado));

        RespostaProdutoDTO respostaProdutoDTO = new RespostaProdutoDTO();
        respostaProdutoDTO =  obterUltimoProdutoCadastrado(contratacao.getProduto());

        respostaCarteiraClienteDTO.setProduto(respostaProdutoDTO);

        return respostaCarteiraClienteDTO;
    }

    private double formatarDouble(double valorProjetado) {
        DecimalFormat df = new DecimalFormat(".##");
        return Double.valueOf(df.format(valorProjetado).replace(',', '.'));
    }

    private RespostaProdutoDTO obterUltimoProdutoCadastrado(Produto produto) {
        RespostaProdutoDTO respostaProdutoDTO = new RespostaProdutoDTO();

        respostaProdutoDTO = produtoService.converterProdutoParaRespostaProdutoDto(produto);
        return respostaProdutoDTO;
    }


    private Contratacao converterCadastroCarteiraDtoParaCadastro(CadastroCarteiraDTO cadastroCarteiraDTO) {
        Contratacao contratacao = new Contratacao();
        contratacao.setPrazoAplicacao(cadastroCarteiraDTO.getPrazoAplicacao());
        contratacao.setValorAplicacao(cadastroCarteiraDTO.getValorAplicacao());
        return contratacao;
    }

}
