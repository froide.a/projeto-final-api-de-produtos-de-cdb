package com.investimento.cdb.services;

import com.investimento.cdb.DTOS.CadastroClienteDTO;
import com.investimento.cdb.DTOS.CadastroProdutoDTO;
import com.investimento.cdb.DTOS.RespostaClienteDTO;
import com.investimento.cdb.DTOS.RespostaProdutoDTO;
import com.investimento.cdb.models.Cliente;
import com.investimento.cdb.models.Produto;
import com.investimento.cdb.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    ProdutoRepository produtoRepository;

    public RespostaProdutoDTO cadastrarProduto(CadastroProdutoDTO cadastroProdutoDTO) {
        try {
            Produto produto = this.converterCadastroProdutoDtoParaProduto(cadastroProdutoDTO);
            produto = produtoRepository.save(produto);

            RespostaProdutoDTO respostaProdutoDTO = this.converterProdutoParaRespostaProdutoDto(produto);

            return respostaProdutoDTO;

        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao incluir cliente: " + e.getMessage());
        }
    }

    public List<RespostaProdutoDTO> consultarTodosProdutos() {
        try {
            List<RespostaProdutoDTO> listaRespostaProdutoDTO = new ArrayList<>();
            List<Produto> listaProduto = produtoRepository.findAll();

            listaRespostaProdutoDTO = this.converterListaProdutoParaListaProdutoDTO(listaProduto);

            return listaRespostaProdutoDTO;
        } catch (RuntimeException e ) {
            throw new RuntimeException("Erro ao listar todos os produtos: " + e.getMessage());
        }

    }

    public RespostaProdutoDTO consultarProdutoPorId(Long id) {
        try {
            Optional<Produto> produto = produtoRepository.findById(id);

            if (produto.isPresent()) {
                RespostaProdutoDTO respostaProdutoDTO = this.converterProdutoParaRespostaProdutoDto(produto.get());
                return respostaProdutoDTO;
            } else {
                throw new RuntimeException("Id do produto não encontrado");
            }
        } catch (RuntimeException e ) {
            throw new RuntimeException("Erro ao consultar o produto por id: " + e.getMessage());
        }
    }

    public RespostaProdutoDTO atualizarProduto(long id, CadastroProdutoDTO cadastroProdutoDTO) {
        try {
            Optional<Produto> produto = produtoRepository.findById(id);
            if (produto.isPresent()) {
                Produto produtoAtualizado = this.converterCadastroProdutoDtoParaProduto(cadastroProdutoDTO);
                produtoAtualizado.setId(id);
                return this.converterProdutoParaRespostaProdutoDto(produtoRepository.save(produtoAtualizado));

            } else {
                throw new RuntimeException("Produto não encontrado para atualização.");
            }
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao consultar o produto por id: " + e.getMessage());
        }
    }

    public void excluirProduto(long id) {
        try {
            produtoRepository.deleteById(id);
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao excluir o produto por id: " + e.getMessage());

        }
    }

    // Métodos de Conversão
    public RespostaProdutoDTO converterProdutoParaRespostaProdutoDto(Produto produto) {
        RespostaProdutoDTO respostaProdutoDTO = new RespostaProdutoDTO();
        respostaProdutoDTO.setId(produto.getId());
        respostaProdutoDTO.setGrauDeRisco(produto.getGrauDeRisco().getDescricao());
        respostaProdutoDTO.setNome(produto.getNome());
        respostaProdutoDTO.setPercentualRentabilidadeAno(produto.getPercentualRentabilidadeAno());
        respostaProdutoDTO.setPrazoMaximoAplicacao(produto.getPrazoMaximoAplicacao());
        respostaProdutoDTO.setPrazoMinimoAplicacao(produto.getPrazoMinimoAplicacao());
        respostaProdutoDTO.setValorMinimoAplicacao(produto.getValorMinimoAplicacao());
        return respostaProdutoDTO;
    }

    public Produto converterCadastroProdutoDtoParaProduto(CadastroProdutoDTO cadastroProdutoDTO) {
        Produto produto = new Produto();
        produto.setNome(cadastroProdutoDTO.getNome());
        produto.setGrauDeRisco(cadastroProdutoDTO.getGrauDeRisco());
        produto.setPercentualRentabilidadeAno(cadastroProdutoDTO.getPercentualRentabilidadeAno());
        produto.setPrazoMaximoAplicacao(cadastroProdutoDTO.getPrazoMaximoAplicacao());
        produto.setPrazoMinimoAplicacao(cadastroProdutoDTO.getPrazoMinimoAplicacao());
        produto.setValorMinimoAplicacao(cadastroProdutoDTO.getValorMinimoAplicacao());
        return produto;
    }

    private List<RespostaProdutoDTO> converterListaProdutoParaListaProdutoDTO(List<Produto> listaProduto) {
        List<RespostaProdutoDTO> listaRespostaProdutoDTO = new ArrayList<>();
        for (Produto produto : listaProduto) {
            RespostaProdutoDTO respostaProdutoDTO = new RespostaProdutoDTO();
            respostaProdutoDTO.setId(produto.getId());
            respostaProdutoDTO.setValorMinimoAplicacao(produto.getValorMinimoAplicacao());
            respostaProdutoDTO.setPrazoMinimoAplicacao(produto.getPrazoMinimoAplicacao());
            respostaProdutoDTO.setPrazoMaximoAplicacao(produto.getPrazoMaximoAplicacao());
            respostaProdutoDTO.setPercentualRentabilidadeAno(produto.getPercentualRentabilidadeAno());
            respostaProdutoDTO.setNome(produto.getNome());
            respostaProdutoDTO.setGrauDeRisco(produto.getGrauDeRisco().getDescricao());
            listaRespostaProdutoDTO.add(respostaProdutoDTO);
        }
        return listaRespostaProdutoDTO;
    }


}
