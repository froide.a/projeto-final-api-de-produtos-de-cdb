package com.investimento.cdb.service;

import com.investimento.cdb.DTOS.CadastroClienteDTO;
import com.investimento.cdb.DTOS.RespostaClienteDTO;
import com.investimento.cdb.models.Cliente;
import com.investimento.cdb.repositories.ClienteRepository;
import com.investimento.cdb.services.ClienteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ClienteTeste {

    @MockBean
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    @MockBean
    private BCryptPasswordEncoder encoder;

    Cliente cliente;

    CadastroClienteDTO cadastroClienteDTO ;

    @BeforeEach
    public void setUp() {
        cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Teste");
        cliente.setCpf("905.305.520-78");
        cliente.setEmail("teste@teste.com.br");
        cliente.setTelefone("11 99999-9999");
        cliente.setSenha("senha");

        cadastroClienteDTO = new CadastroClienteDTO();
        cadastroClienteDTO.setNome("Teste01");
        cadastroClienteDTO.setCpf("204.592.220-07");
        cadastroClienteDTO.setEmail("teste01@teste.com.br");
        cadastroClienteDTO.setTelefone("11 99999-9999");
        cadastroClienteDTO.setSenha("senha");
    }

    @Test
    public void testarConsultarClientePorId() {
        Optional<Cliente> clienteOptional = Optional.of(cliente);
        Mockito.when(clienteRepository.findById(Mockito.anyLong())).thenReturn(clienteOptional);

        RespostaClienteDTO clienteObjeto = clienteService.consultarClientePorId(1l);
        Assertions.assertEquals(cliente.getCpf(), clienteObjeto.getCpf());
    }

    @Test
    public void testarConsultarClientePorIdNaoExiste() {
        Optional<Cliente> clienteOptional = Optional.empty();
        Mockito.when(clienteRepository.findById(Mockito.anyLong())).thenReturn(clienteOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {
            clienteService.consultarClientePorId(2l);
        });
    }

    @Test
    public void testarConsultarTodosClientes() {
        List<Cliente> clientes = new ArrayList<>();
        clientes.add(cliente);

        Mockito.when(clienteRepository.findAll()).thenReturn(clientes);

        List<RespostaClienteDTO> clientesObjeto = clienteService.consultarTodosClientes();
        Assertions.assertEquals(clientes.get(0).getCpf(), clientesObjeto.get(0).getCpf());
    }

    @Test
    public void testarCadastrarCliente() {
        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).then(objeto -> objeto.getArgument(0));
        Mockito.when(encoder.encode(Mockito.anyString())).thenReturn("xxx");
        RespostaClienteDTO respostaClienteDTO =  clienteService.cadastrarCliente(cadastroClienteDTO);
        Assertions.assertEquals("Teste01", respostaClienteDTO.getNome());

        //Verifica se o método encoder.encode foi chamado uma vez
        Mockito.verify(encoder, Mockito.times(1)).encode(Mockito.anyString());

    }

}
